<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    protected $dates = ['dob', 'deleted_at'];
    protected $table = 'persons';
    protected $casts = [ 'meta' => 'array'];

    protected $fillable = ['person_name', 'email', 'country', 'state', 'phone', 'cellphone', 'address',
                                'identity', 'credit'];

    public function documents(){
        return $this->hasMany(Document::class);
    }

    public function personType(){
        return $this->belongsTo(PersonType::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
