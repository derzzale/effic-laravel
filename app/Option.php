<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $casts = [
        'value' => 'array',
        'attr'  => 'array',
        'extra' => 'array'
    ];
}
