<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StorePerson extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'person_type'   => 'required',
            'identity_type'   => 'required',
            'identity'      => 'required|numeric',
            'store'         => 'required',
            'person_name'   => 'required|unique:persons,person_name,'.$this->route('id'),
            'credit'        => 'required',
            'email'         => 'email|required',
            'phone'         => '',
            'cellphone'     => 'required|numeric',
            'address'       => 'required',
            'state'         => 'required',
            'country'       => '',
        ];
    }
}
