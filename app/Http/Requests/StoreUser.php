<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreUser extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $rules['username'] = 'required';
        $rules['first_name'] = 'required';
        $rules['last_name'] = 'required';
        $rules['role'] = 'required';
        $rules['email'] = 'required|unique:users';

        if($this->new_pass != null){
            $rules['new_pass'] = 'required|confirmed';
            $rules['new_pass_confirmation'] = 'required';
        }

        return $rules;
    }
}
