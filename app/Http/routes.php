<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});

// Authentication routes...
Route::get('admin/login', [
    'uses'  => 'Auth\AuthController@getLogin',
    'as'    => 'login'
]);
Route::post('admin/login','Auth\AuthController@postLogin');
Route::get('admin/logout', [
    'uses'  => 'Auth\AuthController@getLogout',
    'as'    => 'logout'
]);

// Registration routes...
Route::get('admin/register', [
    'uses'  => 'Auth\AuthController@getRegister',
    'as'    => 'register'
]);
Route::post('admin/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('admin/recovery-password', [
        'uses'      =>      'Auth\PasswordController@getEmail',
        'as'        =>      'recovery']);
Route::post('admin/recovery-password', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('admin/reset/{token}', [
        'uses'      =>      'Auth\PasswordController@getReset',
        'as'        =>      'pass-reset'
]);
Route::post('admin/reset/{token}', 'Auth\PasswordController@postReset');


/*
 * Routes application
 *
 * */

Route::group(['middleware'=> 'auth','prefix' => 'admin', 'as' => 'admin::'], function (){
    // Admin Panel..
    Route::get('/', [
        'uses'      => 'AdminController@index',
        'as'        => 'dashboard'
    ]);

    // Person Controller

    /**
     * List a Table Persons
     * @param $type Person Type
     * @return Response Page list Datatables
     */
    Route::get('{type}', [
        'uses'      => 'PersonsController@index',
        'as'        => 'persons'
    ]);

    /*
     * Create a Person Type
     */
    Route::get('{type}/create', [
        'uses'      => 'PersonsController@create',
        'as'        => 'person-create'
    ]);

    /*
     * Save Person on Database
     */
    Route::put('person/{type}', [
        'uses'      => 'PersonsController@store',
        'as'        => 'persons-save'
    ]);

    /*
     * Edit a Person
     */
    Route::get('{type}/{id}', [
        'uses'      => 'PersonsController@edit',
        'as'        => 'person-edit'
    ]);

    /*
     * Update a Person
     */
    Route::post('{type}/{id}',[
        'uses' => 'PersonsController@update',   'as' => 'person-update'
    ]);


    //Users
    Route::get('users', [
        'uses' => 'UsersController@index',      'as' => 'users'
    ]);

    Route::get('user/create', [
        'uses' => 'UsersController@create',     'as' => 'user-create'
    ]);

    Route::put('users', [
        'uses' => 'UsersController@store',      'as' => 'user-store'
    ]);

    Route::get('user/{id}', [
        'uses' => 'UsersController@edit',       'as' => 'user-edit'
    ]);

    Route::put('user/{id}', [
        'uses' => 'UsersController@update',     'as' => 'user-update'
    ]);

    //Documents
    Route::get('doc', 'DocumentsController@index');
    Route::get('doc/create', 'DocumentsController@create');


});

