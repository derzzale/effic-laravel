<?php

namespace App\Http\Controllers;

use App\DataTables\PersonsDataTable;
use App\Http\Requests\StorePerson;
//use Illuminate\Http\Request;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Person;
use App\Option;
use App\PersonType;

use Illuminate\Support\Facades\Auth;

class PersonsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PersonsDataTable $dataTable, $type){

        view()->share([
            'hTitle' => 'Persons'
        ]);

        return $dataTable->personType($type)
            ->render('tpl.partials.tables.persons.index', compact('type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        return view('tpl.create-person', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePerson $request, $type)
    {
        $PType = PersonType::where('name', $type)->first();

        $person = new Person($request->all());

        $person->user_id = Auth::id();
        $person->person_type_id = $PType->id;
        $person->meta = array_only($request->all(), $PType->value);

        $person->save();

        return redirect()->route('admin::persons',['type' => $type])->with('status', 'Customer Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($type, $id)
    {
        $person = Person::findOrFail($id);

        return view('tpl.update-person', compact('person','type') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePerson $request,$type, $id)
    {
        $PType  = PersonType::where('name', $type)->first();
        $person = Person::findOrFail($id);

        $person->meta = array_only($request->all(), $PType->value);
        $person->update($request->all());
        //dd($person);

        return redirect()->route('admin::person-edit',[$type,$id])->with('status', 'Customer updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
