<?php

namespace App\Http\Controllers;

use App\Document;

use Illuminate\Http\Request;

use App\Option;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Option::all()->toArray();

        $opts =[];//create array container

        //foreach for read data
        for($i = 0; $i < count($data); $i++){
            $opts[$data[$i]['name']] = [
                'id' => $data[$i]['id'],
                'name' => $data[$i]['name'],
                'value' => json_decode($data[$i]['value'],true),
            ];
        }

        return view('tpl.create-document', compact('docTypes','stores','opts'));

        return '<h1>Welcome to Index Docs</h1>';

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Option::all()->toArray();

        $opts =[];//create array container

        //foreach for read data
        for($i = 0; $i < count($data); $i++){
            $opts[$data[$i]['name']] = [
                'id' => $data[$i]['id'],
                'name' => $data[$i]['name'],
                'value' => json_decode($data[$i]['value'],true),
            ];
        }

        return view('tpl.create-document', compact('docTypes','stores','opts'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
