<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Requests\StoreUser;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    private $userDefault = 'user';
    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        view()->share([
            'singularName'  => 'User',
            'pluralName'    => 'Users',
        ]);
    }

    public function index(UsersDataTable $dataTable)
    {

        view()->share([
            'hTitle'        => 'List Users',
            'addButton'     => true,
        ]);

        return $dataTable
            ->render('tpl.partials.tables.persons.index');
    }

    public function create()
    {
        view()->share([
            'hTitle' => 'Create User',
            'cancelButton'     => true,
        ]);

        $roles = $this->roles();

        return view('tpl.create-user',compact('roles'));
    }

    public function store(StoreUser $request)
    {
        $user = new User($request->all());
        $user->role = $this->userDefault;
        $user->save();
        //dd($request);
    }

    public function edit($id)
    {
        view()->share([
            'hTitle' => 'Edit User',
            'cancelButton'     => true,
        ]);

        $user = User::findOrFail($id);
        $roles = $this->roles();

        return view('tpl.edit-user', compact('user','id', 'roles'));
    }

    public function update(StoreUser $request, $id)
    {
        $user = User::findOrFail($id);

        if ($request->new_pass){
            $user->password = Hash::make($request->new_pass);
        }

        $user->update($request->all());

        return redirect()->route('admin::user-edit',[$id])->with('status', 'User updated!');
    }

    public function roles()
    {
        return ['admin'=> 'Admin', 'editor' => 'Editor', 'user' => 'User', 'client' => 'Client'];
    }


}
