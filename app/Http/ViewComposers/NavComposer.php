<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\PersonType;

/**
 * Class NavComposer
 * @package App\Http\ViewComposers
 */
class NavComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $personTypes = PersonType::all();
        $view->with('personTypes', $personTypes);
    }
}