<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'path.to.action.view')
            ->editColumn('username', function($user){
                return '<a href="'.route('admin::user-edit',$user->id).'">'.$user->username.'</a>';
            })
            ->editColumn('role', function($user){
                return ucfirst( $user->role );
            })
            ->editColumn('buttons', function (){
                return '<ul class="icons-list">
						    <li class="dropdown">
							    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							    	<i class="icon-menu9"></i>
							    </a>
							    <ul class="dropdown-menu dropdown-menu-right">
							    	<li><a href="#"><i class="icon-pencil5"></i> Editar</a></li>
							    	<li><a href="#"><i class="icon-trash"></i> Eliminar</a></li>
							    </ul>
							</li>
						</ul>';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->addCheckbox([
                'data' => 'checkbox',
                'name' => 'checkbox',
                'defaultContent' => '<input type="checkbox" name="ids[]" class="styled">',
                'title' => ''
            ])
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'username',
            'first_name',
            'email',
            'role',
            'created_at',
            'buttons'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatables_' . time();
    }

    protected function getBuilderParameters()
    {
        return [
            'autoWidth' => false,
            'columnDefs' => [
                [ 'targets' => 0, 'className' => 'noVis', ],
                [ 'targets' => 1, 'visible' => false],
            ],
            'stateSave' => true,
            'lengthMenu' => [
                [ 10, 25, 50, -1 ],
                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            'dom' => '<"datatable-header"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            'language' => [
                'search' => '<span>Filtrar:</span> _INPUT_',
            ],
            'buttons' =>  [
                [ 'extend' => 'colvis', 'text' => '<i class="icon-grid"></i>', 'className' => 'btn btn-default', 'columns' => ':visible:not(.noVis)'],
                [
                    'extend' => 'collection',
                    'text' => '<i class="icon-file-download"></i>',
                    'className' => 'btn btn-default',
                    'buttons' => [
                        ['extend' => 'csv', 'text' => '<i class="icon-file-text2"></i><span> CSV</span>'],
                        ['extend' => 'excel', 'text' => '<i class="icon-file-excel"></i><span> Excel</span>'],
                        ['extend' => 'pdf', 'text' => '<i class="icon-file-pdf"></i><span> PDF</span>'],
                        ['extend' => 'print', 'text' => '<i class="icon-printer2"></i><span> Imprimir</span>'],
                        ['extend' => 'copy', 'text' => '<i class="icon-copy3"></i><span> Copiar</span>'],
                    ]
                ],
                [ 'extend' => 'reload', 'text' => '<i class="icon-loop3"></i>', 'className' => 'btn btn-default btn-icon' ],
                [
                    'extend' => 'pageLength', 'text' => '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    'className' => 'btn bg-blue btn-icon'
                ],
            ],
            'drawCallback' => "function(){ 
                $('.styled').uniform({
                radioClass: 'choice', wrapperClass: 'border-primary text-primary'});
            }",
        ];
    }
}
