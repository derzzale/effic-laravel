<?php

namespace App\DataTables;

use App\Person;
use App\PersonType;
use Yajra\Datatables\Services\DataTable;

class PersonsDataTable extends DataTable
{

    /*
     * Custom method
     */
    protected $person_type;

    public function personType($type){
        $types = PersonType::where('name',$type)->get()->first();

        $this->person_type = $types->id;
        return $this;
    }


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('created_at', function ($user) {
                return $user->created_at->diffForHumans() ;
            })
            ->editColumn('person_name', function ($person){
                return '<a href="'.route('admin::person-edit', [$person->personType->name,$person->id]).'">'.$person->person_name.'</a>';
            })
            ->editColumn('status', function ($person){
                switch ($person->status):
                    default:
                        return '<a href="#">'.$person->status.'</a>';
                endswitch;
            })
            ->editColumn('buttons', function ($person){
                return '<ul class="icons-list">
						    <li class="dropdown">
							    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							    	<i class="icon-menu9"></i>
							    </a>
							    <ul class="dropdown-menu dropdown-menu-right">
							    	<li><a href="'.route('admin::person-edit', [$person->personType->name,$person->id]).'"><i class="icon-pencil5"></i> Editar</a></li>
							    	<li><a href="#"><i class="icon-trash"></i> Eliminar</a></li>
							    </ul>
							</li>
						</ul>';
            })
            ->make(true);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Person::with('user')->where('person_type_id',$this->person_type );
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->addCheckbox([
                        'data' => 'checkbox',
                        'name' => 'checkbox',
                        'defaultContent' => '<input type="checkbox" name="ids[]" class="styled">',
                        'title' => ''
                    ])
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID','width' => 10, 'orderable' => false, 'searchable' => false],
            ['data' => 'person_name', 'name' => 'person_name', 'title' => 'Nombre'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Creado','width' => 150, 'searchable' => false],
            ['data' => 'user.username', 'name' => 'username', 'title' => 'User', 'width' => 40, 'searchable' => false],
            ['data' => 'status', 'name' => 'status', 'title' => 'Estado', 'width' => 40, 'searchable' => false],
            ['data' => 'buttons', 'name' => 'buttons', 'title' => '', 'width' => 30, 'orderable' => false, 'searchable' => false],
        ];

    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'persons_' . date('Y-m-d-G-i-s');
    }

    protected function getBuilderParameters()
    {
        return [

            'autoWidth' => false,
            'order' => [
                [ 3, 'desc' ],
            ],
            'columnDefs' => [
                [ 'targets' => 0, 'className' => 'noVis', ],
                [ 'targets' => 1, 'visible' => false],
            ],
            'stateSave' => true,
            'lengthMenu' => [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            'dom' => '<"datatable-header"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            'language' => [
                'search' => '<span>Filtrar:</span> _INPUT_',
            ],
            'buttons' =>  [
                [ 'extend' => 'colvis', 'text' => '<i class="icon-grid"></i>', 'className' => 'btn btn-default', 'columns' => ':visible:not(.noVis)'],
                [
                    'extend' => 'collection',
                    'text' => '<i class="icon-file-download"></i>',
                    'className' => 'btn btn-default',
                    'buttons' => [
                        ['extend' => 'csv', 'text' => '<i class="icon-file-text2"></i><span> CSV</span>'],
                        ['extend' => 'excel', 'text' => '<i class="icon-file-excel"></i><span> Excel</span>'],
                        ['extend' => 'pdf', 'text' => '<i class="icon-file-pdf"></i><span> PDF</span>'],
                        ['extend' => 'print', 'text' => '<i class="icon-printer2"></i><span> Imprimir</span>'],
                        ['extend' => 'copy', 'text' => '<i class="icon-copy3"></i><span> Copiar</span>'],
                    ]
                ],
                [ 'extend' => 'reload', 'text' => '<i class="icon-loop3"></i>', 'className' => 'btn btn-default btn-icon' ],
                [
                    'extend' => 'pageLength', 'text' => '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    'className' => 'btn bg-blue btn-icon'
                ],
            ],
            'drawCallback' => "function(){ $('.styled').uniform({
                radioClass: 'choice', wrapperClass: 'border-primary text-primary'})  }",
        ];
    }

}
