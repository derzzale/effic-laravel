<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonType extends Model
{
    protected $fillable = ['id','name', 'value'];

    protected $casts = [
        'value' => 'array',
    ];

    public function persons(){
        $this->hasMany(Person::class);
    }
}
