<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\PersonType;
use App\Option;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'tpl.partials.navigation', 'App\Http\ViewComposers\NavComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
