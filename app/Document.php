<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    //
    public function person(){
        return $this->belongsTo(Person::class);
    }

    public function  document_type(){
        return $this->belongsTo(DocumentType::class);
    }

    public function serie(){
        return $this->belongsTo(Serie::class);
    }

    public function  store(){
        return $this->belongsTo(Store::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
