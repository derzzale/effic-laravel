<div id="field_{{ $id }}"{!! Html::classes(['form-group','has-feedback', 'has-error' => $hasErrors]) !!}>
    <label for="{{ $id }}" class="control-label col-lg-3 cursor-pointer">
        {{ $label }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <div class="col-lg-9">
        {!! $input !!}
        @foreach ($errors as $error)
            <div class="form-control-feedback">
                <i class="icon-notification2"></i>
            </div>
            <span class="help-block">{{ $error }}</span>
        @endforeach
    </div>
</div>