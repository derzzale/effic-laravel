@if( isset($col) )
<div class="col-lg-{{ $col }}">
@endif
    <div id="field_{{ $id }}"{!! Html::classes(['form-group', 'has-error' => $hasErrors,'has-feedback', (isset($ileft)?'has-feedback-left':'' )]) !!}>
        @if ($label != '')
            <label for="{{ $id }}" class="control-label">
                {{ $label }}
            </label>
            @if ($required)
                <span class="text-danger">*</span>
            @endif
        @endif

        {!! $input !!}
        @foreach ($errors as $error)
            <p class="help-block">{{ $error }}</p>
        @endforeach

        @if( isset($icon) )
            <div class="form-control-feedback">
                <i class="{{ $icon }} text-muted"></i>
            </div>
        @endif
    </div>
@if( isset($col) )
</div>
@endif