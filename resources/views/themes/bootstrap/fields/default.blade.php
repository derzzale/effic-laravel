@if( isset($col) )
    <div class="col-lg-{{ $col }}">
@endif
        <div id="field_{{ $id }}"{!! Html::classes(['form-group', 'has-error' => $hasErrors,'has-feedback', (isset($ileft)?'has-feedback-left':'' )]) !!}>
            @if ($label != '')
                <label for="{{ $id }}" class="control-label">
                    {{ $label }}
                    @if ($required)
                        <span class="text-danger">*</span>
                    @endif
                </label>
            @endif

            @if( isset($icon) )
                <div class="input-group">
                   <span class="input-group-addon"><i class="{{ $icon }}"></i></span>
                    {!! $input !!}
                </div>
            @endif

            @foreach ($errors as $error)
                    <label class="validation-error-label" for="field_{{ $id }}">{{ $error }}</label>
            @endforeach
        </div>
@if( isset($col) )
    </div>
@endif