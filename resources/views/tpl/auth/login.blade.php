@extends('tpl.base')
@section('title','Iniciar Sesión')
@section('content')
    <!-- Content area -->
    <div class="content">

        <!-- Simple login form -->
        <form action="{{ route('login') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel panel-body login-form">
                <div class="text-center">
                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                    <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                </div>

                {!! Field::text('username',['label' => '', 'ph' => 'Username','tpl' => ''],['icon' => 'icon-user', 'ileft' => true]) !!}
                {!! Field::password('password',['label' => '', 'ph' => 'Password'],['icon' => 'icon-lock2', 'ileft' => true]) !!}

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
                </div>

                <div class="text-center">
                    <a href="{{ route('recovery') }}">Forgot password?</a>
                </div>
            </div>
        </form>
        <!-- /simple login form -->


        <!-- Footer -->
        <div class="footer text-muted text-center">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="#" target="_blank">Renzo Carlos</a>
        </div>
        <!-- /footer -->

    </div>
    <!-- /content area -->
@endsection