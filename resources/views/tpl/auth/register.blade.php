@extends('tpl.base')
@section('content')
    <!-- Content area -->
    <div class="content">

        <!-- Registration form -->
        <form action="{{ route('register') }}" method="post">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="panel registration-form">
                        @if (count($errors))
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="panel-body">
                            <div class="text-center">
                                <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                                <h5 class="content-group-lg">Create account <small class="display-block">All fields are required</small></h5>
                            </div>

                            <div class="form-group has-feedback">
                                <input name="username" type="text" class="form-control" placeholder="Choose username">
                                <div class="form-control-feedback">
                                    <i class="icon-user-plus text-muted"></i>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input name="firts_name" type="text" class="form-control" placeholder="First name" value="{{ old('firts_name') }}">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-check text-muted"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input name="last_name" type="text" class="form-control" placeholder="Second name" value="{{ old('last_name') }}">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-check text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input name="password" type="password" class="form-control" placeholder="Create password">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-lock text-muted"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input name="password_confirmation" type="password" class="form-control" placeholder="Repeat password">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-lock text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input name="email" type="email" class="form-control" placeholder="Your email">
                                        <div class="form-control-feedback">
                                            <i class="icon-mention text-muted"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input name="email_confirmation" type="email" class="form-control" placeholder="Repeat email">
                                        <div class="form-control-feedback">
                                            <i class="icon-mention text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Back to login form</button>
                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /registration form -->


        <!-- Footer -->
        <div class="footer text-muted text-center">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
        </div>
        <!-- /footer -->

    </div>
    <!-- /content area -->
@endsection