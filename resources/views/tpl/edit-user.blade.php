@extends('tpl.base')
@section('title','Crear Cliente')
@section('content')
    <!-- Content area -->
    <div class="content">
        @include('alerts')
        <div class="panel panel-flat">
            <div class="panel-body">
                {!! Form::model($user,['route' => ['admin::user-update', $id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                    @include('tpl.partials.forms.userdata')
                    @include('tpl.partials.forms.userchangepass')
                    <div class="text-right">
                        {!! Form::submit("Update User",['class' => 'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /content area -->
@endsection
