@extends('tpl.base')
@section('title','Dashboard')
@section('content')
    <!-- Content area -->
    <div class="content">
        <h1>Bienvenido
        @if (Auth::user())
            {{  Auth::user()->username }}
        @endif
        </h1>
    </div>
    <!-- /content area -->
@endsection