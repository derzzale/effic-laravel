<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <title>@yield('title')</title>
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css"/>
        <!-- /global stylesheets -->
    </head>
    <body class="{{ ( Auth::check() ) ? '':'login-container' }}">
        @if( Auth::check() )
            <!-- Main navbar -->
            @include('tpl.partials.main-header')
            <!-- /main navbar -->
        @endif
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @if( Auth::check() )
                    <!-- Main sidebar -->
                    @include('tpl.partials.navigation')
                    <!-- /main sidebar -->
                @endif
                <!-- Main content -->
                <div class="content-wrapper">
                    @if( Auth::check() )
                    <!-- Page header -->
                    <div class="page-header page-header-default page-header-xs">
                        <!-- Page header content -->
                        <div class="page-header-content">
                            <div class="page-title">
                                <h2>
                                    <i class="icon-stack2 position-left"></i> <span class="text-semibold"></span>{{ (isset($hTitle)) ? $hTitle:'No Title' }}
                                    <small class="display-block">{{ (isset($hSubtitle)) ? $hSubtitle:'' }}</small>
                                </h2>
                            </div>
                            @if(isset($addButton))
                                <div class="heading-elements">
                                    <a href="#" class="btn btn-primary heading-btn">New {{ $singularName }}</a>
                                </div>
                            @endif
                            @if(isset($cancelButton))
                                <div class="heading-elements">
                                    <a href="#" class="btn btn-danger heading-btn">Cancel</a>
                                </div>
                            @endif
                            @if(Route::currentRouteName() == 'admin::persons')
                                <div class="heading-elements">
                                    <a href="{{ route('admin::person-create',[$type]) }}" class="btn btn-primary heading-btn">Añadir nuevo</a>
                                    <a class="btn btn-primary heading-btn buttons-csv">Custom</a>
                                </div>
                            @endif
                        </div>
                        <!-- /page header content -->
                    </div>
                    <!-- /page header -->
                    @endif
            @yield('content')
                </div>
                <!-- /main content -->
            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
        <!-- Core JS files -->
        <script src="{{ asset('assets/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/core/app.js') }}" type="text/javascript"></script>
        <!-- /core JS files -->
        <!-- Theme JS files -->
        <script src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/uniform.min.js') }}" type="text/javascript"></script>
        <!-- Theme JS files -->
        @stack('scripts')
    </body>
</html>