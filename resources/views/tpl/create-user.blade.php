@extends('tpl.base')
@section('title','Crear Cliente')
@section('content')
    <!-- Content area -->
    <div class="content">
        @include('alerts')
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h3 class="panel-title">Personal Info</h3>
            </div>
            <div class="panel-body">
                {!! Form::open(['route' => ['admin::user-store'], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                    @include('tpl.partials.forms.userdata')
                    @include('tpl.partials.forms.userchangepass')
                    <div class="text-right">
                        {!! Form::submit("Create User",['class' => 'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h3 class="panel-title">Business Data</h3>
            </div>
            <div class="panel-body">

                {!! Form::open(['route' => ['admin::persons-save','clientes'], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @include('tpl.partials.forms.person-form')
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <!-- /content area -->
@endsection
