@extends('tpl.base')
@section('title','Crear Documento')
@section('content')
 

<!-- Content area -->
    <div class="content">
        @include('alerts')
        {!! Form::open(['route' => 'admin::persons', 'method' => 'put']) !!}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    Datos de Facturación
                    <small class="display-block">
                        Ingresa los Datos del Documento
                    </small>
                </h5>
            </div>
            <div class="panel-body">
                @include('tpl.partials.forms.documentdata-form')
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">
                    Datos de Cliente
                    <small class="display-block">
                        Ingresa los Datos del Cliente
                    </small>
                </h5>
            </div>
            <div class="panel-body">
                @include('tpl.partials.forms.documentclient-form')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            Datos de Venta
                            <small class="display-block">
                                Ingresa los Productos o Servicios
                            </small>
                        </h5>
                        <div class="heading-elements">
                            <button class="btn btn-danger" data-repeater-create="" type="button">
                                <i class="icon icon-plus2">
                                </i>
                            </button>
                        </div>
                        <a class="heading-elements-toggle">
                            <i class="icon-more">
                            </i>
                        </a>
                    </div>
                    <div class="panel-body">
                        @include('tpl.partials.forms.documentitems-form')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            Importe total
                        </h5>
                    </div>
                    <div class="panel-body">
                        @include('tpl.partials.forms.documenttotals')
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
<!-- /content area -->
@endsection
