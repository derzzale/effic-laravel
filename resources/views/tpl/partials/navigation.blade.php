<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li><a href="{{ route('admin::dashboard') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    <li>
                        <a href="#"><i class="icon-user"></i> <span>Documentos</span></a>
                        <ul>
                            <li><a href="{{ url('admin/doc') }}">Documentos</a></li>
                            <li><a href="{{ url('admin/doc/create') }}">Añadir Nuevo</a></li>
                        </ul>
                    </li>
                    @foreach( $personTypes as $pType)
                     <li>
                         <a href="#"><i class="icon-user"></i> <span>{{ $pType->name }}</span></a>
                         <ul>
                             <li><a href="{{ route('admin::persons', $pType->name) }}">{{ $pType->name }}</a></li>
                             <li><a href="{{ route('admin::person-create',$pType->name) }}">Añadir Nuevo</a></li>
                         </ul>
                     </li>
                    @endforeach
                    <li>
                        <a href="#"><i class="icon-user"></i> <span>Users</span></a>
                        <ul>
                            <li><a href="{{ route('admin::users') }}">Users</a></li>
                            <li><a href="#">Add New</a></li>
                        </ul>
                    </li>
                    <!-- /main -->

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>