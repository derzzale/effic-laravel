@if (session('status'))
<div class="alert alert-success alert-styled-left">
    {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
</div>
@endif