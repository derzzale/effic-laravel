<div class="row">
    {!! Field::select('store_id',['A' => 'A'],null, ['label' => 'Tienda'],['col' => 3, 'icon' => 'icon-store']) !!}
    {!! Field::select('status',$opts['status']['value'],
        null,['label' => 'Estado'],['col' => 3, 'icon' => 'icon-keyboard']) !!}
    {!! Field::select('payment',['A' => 'A'], null, ['label' => 'Tipo de Pago'],['col' => 3, 'icon' => 'icon-keyboard']) !!}
    {!! Field::select('currency',$opts['currency']['value'], null, ['label' => 'Moneda'],['col' => 3, 'icon' => 'icon-keyboard']) !!}
</div>
<div class="row">
    {!! Field::select('documentType',['A' => 'B'],null,['label' => 'Tipo de Documento'],['col' => 3, 'icon' => 'icon-keyboard']) !!}
    {!! Field::select('serie',['1' => '001','2' => '002'], null, ['label' => 'Serie'],['col' => 2, 'icon' => 'icon-keyboard']) !!}
    {!! Field::text('correlativo',['required'],['icon' => 'icon-keyboard', 'col' => 3]) !!}
    {!! Field::text('datecreated',['required'],['icon' => 'icon-keyboard', 'col' => 2]) !!}
    {!! Field::text('dateEnd',['required'],['icon' => 'icon-keyboard', 'col' => 2]) !!}
</div>
<div class="row">
    {!! Field::file('aaa',['label' => 'Archivo Adjunto'],['col' => 6, 'icon' => 'icon-keyboard']) !!}
</div>