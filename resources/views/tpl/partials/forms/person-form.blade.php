<fieldset class="content-group">
    <legend class="text-bold">User Data</legend>
    {!! Field::select('identity_type',['dni' => 'DNI', 'ruc' => 'RUC'],null,['tpl' => 'themes.bootstrap.fields.horizontal','required'])!!}
    {!! Field::select('store',['tienda-1' => 'Tienda 1', 'tienda-2' => 'Tienda 2'],null,['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('identity',['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('person_name', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('credit', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::email('email', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('cellphone', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('phone', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('address', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('state', ['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}

{!! Form::submit('Crear Cliente',['class' => 'btn btn-success'])  !!}
</fieldset>