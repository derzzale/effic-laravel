<div class="row">
    <table class="table table-xxs">
        <thead>
            <tr>
                <th width="100">
                    Cantidad
                </th>
                <th>
                    Producto
                </th>
                <th width="130">
                    Precio Unitario
                </th>
                <th width="130">
                    Total
                </th>
                <th>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {!! Field::text('cantidad',['required','label' => '','ph' => 'Canti']) !!}
                </td>
                <td>
                    {!! Field::text('producto',['required','label' => '','ph' => 'nombre del producto']) !!}
                </td>
                <td>
                    {!! Field::text('precioUnidad',['required','label' => '']) !!}
                </td>
                <td>
                    {!! Field::text('total',['required','label' => '']) !!}
                </td>
                <td>
                </td>
            </tr>
        </tbody>
    </table>
</div>