<fieldset class="content-group">
    <legend class="text-bold">Security</legend>
    {!! Field::text('new_pass',['tpl' => 'themes.bootstrap.fields.horizontal']) !!}
    {!! Field::text('new_pass_confirmation',['tpl' => 'themes.bootstrap.fields.horizontal']) !!}
</fieldset>
