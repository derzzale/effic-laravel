{!! Field::text('subtotal',['label' => 'SUBTOTAL', 'required'],['icon' => 'icon-keyboard', 'col' => 12]) !!}
{!! Field::text('tax_value',['label' => 'IGV', 'required'],['icon' => 'icon-keyboard', 'col' => 12]) !!}
{!! Field::text('total',['label' => 'TOTAL', 'required'],['icon' => 'icon-keyboard', 'col' => 12]) !!}