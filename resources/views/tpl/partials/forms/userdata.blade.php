<fieldset class="content-group">
    <legend class="text-bold">User Data</legend>
    {!! Field::text('first_name',['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('last_name',['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::text('username',['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::select('role',$roles,['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
    {!! Field::email('email',['tpl' => 'themes.bootstrap.fields.horizontal','required']) !!}
</fieldset>
