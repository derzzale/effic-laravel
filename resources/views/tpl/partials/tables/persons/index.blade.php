@extends('tpl.base')
@section('title','Crear Cliente')
@section('content')
    <!-- Content area -->
    <div class="content">
        @include('alerts')
        <div class="panel panel-flat">
            {!! $dataTable->table(['class' => 'table table-striped']) !!}
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
@endpush