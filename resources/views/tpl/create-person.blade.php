@extends('tpl.base')
@section('title','Crear Cliente')
@section('content')
<!-- Content area -->
<div class="content">
    @include('alerts')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h4 class="panel-title">Crear Cliente</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => ['admin::persons-save',$type], 'method' => 'put', 'class' => 'form-horizontal']) !!}
            @include('tpl.partials.forms.person-form')
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
