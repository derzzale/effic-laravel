@extends('tpl.base')
@section('title','Actualizar Cliente')
@section('content')
    <!-- Content area -->
    <div class="content">
        @include('alerts')
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h4 class="panel-title">Crear Cliente</h4>
            </div>
            <div class="panel-body">
                {!! Form::model($person,['route' => ['admin::person-update',$type,$person->id]]) !!}
                @include('tpl.partials.forms.person-form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /content area -->
@endsection