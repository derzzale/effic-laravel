<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'firts_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'role' => $faker->randomElement(['admin','guest']),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Person::class,function (Faker\Generator $faker){
    return [
        'person_name' => $faker->name,
        'email' => $faker->safeEmail,
        'country' => 'Perú',
        'state' => $faker->randomElement(['Lima','Ica','Arequipa','Junin','Cusco']),
        'phone' => $faker->phoneNumber,
        'cellphone' => $faker->numberBetween(900000000,999999999),
        'address'   => $faker->address,
        'identity'  => $faker->numberBetween(4000000,9999999),
        'user_id'   => $faker->numberBetween(1,2),
        'person_type_id'   =>'1',
    ];
});