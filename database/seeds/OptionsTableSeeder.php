<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'username' => 'admin',
                'email' => 'derzz.ale@gmail.com',
                'password' => bcrypt('shadow'),
                'role' => 'admin',
            ],
            [
                'username' => 'guest',
                'email' => 'demo@demo.com',
                'password' => bcrypt('shadow'),
                'role' => 'user',
            ]
        ]);

        $p_type = collect([ 'store', 'status', 'identity_type' ]);

        DB::table('person_types')->insert([
            ['name' => 'clientes', 'value' => $p_type],
            ['name' => 'alumnos', 'value' => ''],
        ]);


        factory(App\Person::class, 50)->create();
    }
}
