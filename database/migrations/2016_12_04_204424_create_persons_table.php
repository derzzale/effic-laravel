<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {

            $table->increments('id');
            $table->text('person_name');
            $table->string('email');
            $table->string('country');
            $table->string('state');
            $table->string('phone');
            $table->string('cellphone');
            $table->string('address');
            $table->string('identity');
            $table->string('credit');
            $table->string('image');
            $table->string('status')->default('active');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('person_type_id')->nullable()->index();
            $table->text('meta');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('persons');
    }
}
